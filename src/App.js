import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Home from './Home';
import About from './About';


const MyHome = () => <Home />;
const MyAbout = () => <About />;

const AppRouter = () => (
  <Router>
    <div>
      <Route path="/" exact component={MyHome} />
      <Route path="/about/" component={MyAbout} />
    </div>
  </Router>
);

class App extends Component {
  render() {
    return (
      <div>
        <AppRouter />
        {/* <Router>
          <div>
            <Route path="/" exact compononent={MyHome} />
            <Route path="/about/" compononent={MyAbout} />
          </div>
        </Router> */}
      </div>
    );
  }
}

export default App;
