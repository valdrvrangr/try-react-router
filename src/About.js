import React, { Component } from 'react';
import './App.css';

class About extends Component {
  render() {
    return (
      <div className="App">
        <h1>This is the about page.</h1>
      </div>
    );
  }
}

export default About;
