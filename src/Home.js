import React, { Component } from 'react';
import './App.css';

class Home extends Component {
  render() {
    return (
      <div className="App">
        <h1>This is the home page.</h1>
      </div>
    );
  }
}

export default Home;
